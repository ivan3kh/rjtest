#include <iostream>
#include <string_view>
#include "string"
#include "rapidjson/writer.h"
#include "rapidjson/document.h"


int main() {
    std::string json = R"({"data":[]})";
    rapidjson::Document d;
    d.Parse(json.c_str());

    auto data = d.GetObject();
    auto fun = [](const rapidjson::Value &) {
    };
    fun(data); //<<<!!!

    rapidjson::StringBuffer buffer;
    rapidjson::Writer<rapidjson::StringBuffer> writer(buffer);
    d.Accept(writer);
    std::string x = buffer.GetString();

    std::cout << x << std::endl;

    return 0;
}
